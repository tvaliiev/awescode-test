<?php

namespace App\Util\Filter;

use App\Util\Filter\Contracts\FilterContract;
use Illuminate\Database\Eloquent\Builder;
use Spatie\DataTransferObject\DataTransferObject;

class FilterHandler
{
    public static function filter(DataTransferObject $dto, Builder $query): void
    {
        $reflection = new \ReflectionClass($dto::class);
        foreach ($reflection->getProperties() as $property) {
            $attributes = $property->getAttributes(FilterableBy::class);
            if (count($attributes) !== 1) {
                continue;
            }

            /** @var \App\Util\Filter\FilterableBy $attribute */
            $attribute = $attributes[0]->newInstance();

            $attribute->newFilter()->filter($query, $dto, $property->getName());
        }
    }
}
