<?php

namespace App\Util\Filter\Filters;

use App\Util\Filter\Contracts\FilterContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Spatie\DataTransferObject\DataTransferObject;

class SimpleFilter implements FilterContract
{
    public function filter(Builder $query, DataTransferObject $dto, string $property): void
    {
        $target = $dto->{$property};
        if ($target === null) {
            return;
        }


        foreach ((new \ReflectionClass($target))->getProperties(\ReflectionProperty::IS_PUBLIC) as $functionProperty) {
            $functionName = 'filter' . Str::studly($functionProperty->getName()) . 'Property';
            $actualValue = $target->{$functionProperty->getName()};
            if ($actualValue === null || !method_exists($this, $functionName)) {
                continue;
            }

            $this->{$functionName}($query, $property, $actualValue);
        }
    }

    protected function filterEqProperty(Builder $query, string $baseProperty, $value): void
    {
        $query->where($baseProperty, $value);
    }

    protected function filterGtProperty(Builder $query, string $baseProperty, $value): void
    {
        $query->where($baseProperty, '>', $value);
    }

    protected function filterLtProperty(Builder $query, string $baseProperty, $value): void
    {
        $query->where($baseProperty, '<', $value);
    }

    protected function filterLikeProperty(Builder $query, string $baseProperty, $value): void
    {
        $query->where($baseProperty, 'LIKE', "%$value%");
    }
}
