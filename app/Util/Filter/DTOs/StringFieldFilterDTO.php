<?php

namespace App\Util\Filter\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class StringFieldFilterDTO extends DataTransferObject
{
    public ?string $like;
}
