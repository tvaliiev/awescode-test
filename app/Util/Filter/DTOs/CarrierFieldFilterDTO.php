<?php

namespace App\Util\Filter\DTOs;

use App\Enums\CarrierIdentifiers;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\EnumCaster;
use Spatie\DataTransferObject\DataTransferObject;

class CarrierFieldFilterDTO extends DataTransferObject
{
    #[CastWith(EnumCaster::class, CarrierIdentifiers::class)]
    public ?CarrierIdentifiers $eq;
}
