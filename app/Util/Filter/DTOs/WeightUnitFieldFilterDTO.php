<?php

namespace App\Util\Filter\DTOs;

use App\Enums\DeliveryWeightUnitEnum;
use Spatie\DataTransferObject\DataTransferObject;

class WeightUnitFieldFilterDTO extends DataTransferObject
{
    public ?DeliveryWeightUnitEnum $weightUnitEnum;

}
