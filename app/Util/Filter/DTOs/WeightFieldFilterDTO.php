<?php

namespace App\Util\Filter\DTOs;

use App\Util\Filter\Attributes\ShouldConvert;
use App\Util\Filter\FilterableBy;
use Spatie\DataTransferObject\DataTransferObject;

class WeightFieldFilterDTO extends DataTransferObject
{
    #[ShouldConvert]
    public int|float|null $gt;

    #[ShouldConvert]
    public int|float|null $lt;

    #[ShouldConvert]
    public int|float|null $eq;

    public function convertToGrams(): void
    {
        foreach ((new \ReflectionClass($this::class))->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            $propertyName = $property->getName();
            if ($this->{$propertyName} === null || !$property->getAttributes(ShouldConvert::class)) {
                continue;
            }

            $this->{$propertyName} = (int)($this->{$propertyName} * 1000);
        }
    }
}
