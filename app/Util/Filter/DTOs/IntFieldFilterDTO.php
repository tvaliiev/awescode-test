<?php

namespace App\Util\Filter\DTOs;

use App\Util\Filter\FilterableBy;
use Spatie\DataTransferObject\DataTransferObject;

class IntFieldFilterDTO extends DataTransferObject {
    public int|null $gt;
    public int|null $lt;
    public int|null $eq;
}
