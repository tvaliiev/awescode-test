<?php

namespace App\Util\Filter;


use App\Util\Filter\Contracts\FilterContract;
use App\Util\Filter\Filters\SimpleFilter;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class FilterableBy
{
    public function __construct(
        public string $filterClass = SimpleFilter::class,
    )
    {
    }

    public function newFilter(): FilterContract
    {
        return new $this->filterClass();
    }
}
