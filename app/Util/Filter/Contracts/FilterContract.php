<?php

namespace App\Util\Filter\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Spatie\DataTransferObject\DataTransferObject;

interface FilterContract
{
    public function filter(Builder $query, DataTransferObject $dto, string $property): void;
}
