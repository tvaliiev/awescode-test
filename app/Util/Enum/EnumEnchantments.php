<?php

namespace App\Util\Enum;

trait EnumEnchantments
{
    /**
     * Returns enum values as an array.
     */
    public static function valueArray(): array
    {
        return array_column(static::cases(), 'value');
    }

    /**
     * Returns enum values as a list.
     */
    public static function valueList(string $separator = ','): string
    {
        return implode($separator, static::valueArray());
    }
}
