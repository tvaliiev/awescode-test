<?php

namespace App\DTOs\Models;

use App\Enums\CarrierIdentifiers;
use App\Util\Attributes\Updatable;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\EnumCaster;
use Spatie\DataTransferObject\DataTransferObject;

class UpdateDeliveryDTO extends DataTransferObject
{
    public int $id;

    #[Updatable]
    public ?int $weight;

    #[Updatable]
    public ?int $delivery_price;

    #[Updatable]
    public ?string $description;

    #[Updatable]
    #[CastWith(EnumCaster::class, CarrierIdentifiers::class)]
    public ?CarrierIdentifiers $carrier_id;
}
