<?php

namespace App\DTOs\Models;

use App\Enums\CarrierIdentifiers;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\EnumCaster;
use Spatie\DataTransferObject\DataTransferObject;

class CreateDeliveryDTO extends DataTransferObject
{
    public int $weight;

    public string $description;

    #[CastWith(EnumCaster::class, CarrierIdentifiers::class)]
    public CarrierIdentifiers $carrier_id;
}
