<?php

namespace App\DTOs\Models;

use App\Enums\CarrierIdentifiers;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\EnumCaster;
use Spatie\DataTransferObject\DataTransferObject;

#[Schema]
class DeliveryDTO extends DataTransferObject
{
    #[Property]
    public ?int $id;

    #[Property]
    public int $weight;

    #[Property]
    public string $description;

    #[Property]
    public ?int $delivery_price;

    #[Property(ref: "#/components/schemas/CarrierIdentifiers")]
    #[CastWith(EnumCaster::class, CarrierIdentifiers::class)]
    public CarrierIdentifiers $carrier_id;
}
