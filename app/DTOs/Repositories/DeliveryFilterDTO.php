<?php

namespace App\DTOs\Repositories;

use App\Enums\DeliveryWeightUnitEnum;
use App\Util\Filter\DTOs\CarrierFieldFilterDTO;
use App\Util\Filter\DTOs\IntFieldFilterDTO;
use App\Util\Filter\DTOs\StringFieldFilterDTO;
use App\Util\Filter\DTOs\WeightFieldFilterDTO;
use App\Util\Filter\FilterableBy;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\DefaultCast;
use Spatie\DataTransferObject\Casters\EnumCaster;
use Spatie\DataTransferObject\DataTransferObject;


class DeliveryFilterDTO extends DataTransferObject
{
    #[FilterableBy]
    public ?WeightFieldFilterDTO $weight;

    #[CastWith(EnumCaster::class, DeliveryWeightUnitEnum::class)]
    public ?DeliveryWeightUnitEnum $weightUnit;

    #[FilterableBy]
    public ?IntFieldFilterDTO $delivery_price;

    #[FilterableBy]
    public ?CarrierFieldFilterDTO $carrier_id;

    #[FilterableBy]
    public ?StringFieldFilterDTO $description;

    public int $page = 1;

    public int $perPage = 10;
}

