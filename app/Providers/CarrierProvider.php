<?php

namespace App\Providers;

use App\Contracts\Carrier\CarrierContract;
use App\Models\Carriers\CarrierFactory;
use Illuminate\Support\ServiceProvider;

class CarrierProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            CarrierContract::class,
            fn($app, array $params) => CarrierFactory::makeFromIdentifier($params['carrierId']),
        );
    }
}
