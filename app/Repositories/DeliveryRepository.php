<?php

namespace App\Repositories;

use App\DTOs\Models\CreateDeliveryDTO;
use App\DTOs\Models\DeliveryDTO;
use App\DTOs\Models\UpdateDeliveryDTO;
use App\DTOs\Repositories\DeliveryFilterDTO;
use App\Models\Delivery;
use App\Util\Attributes\Updatable;
use App\Util\Filter\FilterHandler;
use Illuminate\Pagination\LengthAwarePaginator;

class DeliveryRepository
{
    public function getList(DeliveryFilterDTO $filter): LengthAwarePaginator
    {
        $query = Delivery::query();

        FilterHandler::filter($filter, $query);

        return $query->paginate(
            perPage: $filter->perPage,
            page: $filter->page,
        )->through(fn (Delivery $delivery) => $delivery->toDto());
    }

    public function store(CreateDeliveryDTO $dto): DeliveryDTO
    {
        $delivery = new Delivery($dto->toArray());
        $delivery->delivery_price = $delivery->calculateDeliveryPrice();

        $delivery->save();

        return $delivery->toDto();
    }

    public function update(UpdateDeliveryDTO $dto): DeliveryDTO
    {
        /** @var Delivery $delivery */
        $delivery = Delivery::findOrFail($dto->id);

        foreach ((new \ReflectionClass($dto::class))->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            $propertyName = $property->getName();
            if ($dto->{$propertyName} === null || count($property->getAttributes(Updatable::class)) === 0 ){
                continue;
            }

            $delivery->{$propertyName} = $dto->{$propertyName};
        }

        $delivery->save();

        return $delivery->toDto();
    }
}
