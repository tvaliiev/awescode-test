<?php

namespace App\Models;

use App\Contracts\Carrier\CarrierContract;
use App\DTOs\Models\DeliveryDTO;
use App\Enums\CarrierIdentifiers;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'carrier_id' => CarrierIdentifiers::class,
    ];

    public function getDeliveryPriceAttribute(): int
    {
        if (array_key_exists('delivery_price', $this->attributes) && $this->attributes['delivery_price'] !== null) {
            return $this->attributes['delivery_price'];
        }

        return $this->calculateDeliveryPrice();
    }


    public function toDto(): DeliveryDTO
    {
        return new DeliveryDTO($this->toArray());
    }

    public function carrier(): Attribute
    {
        return Attribute::make(
            get: fn($value, $attributes) => app()->make(CarrierContract::class, ['carrierId' => $this->carrier_id])
        );
    }

    public function calculateDeliveryPrice(): int
    {
        return $this->carrier->getPriceFor($this->toDto());
    }
}
