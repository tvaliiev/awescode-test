<?php

namespace App\Models\Carriers;

use App\Enums\CarrierIdentifiers;
use App\Exceptions\Carriers\UnknownCarrierException;
use App\Models\Delivery;

class CarrierFactory
{
    public static function make(int $carrierId): Carrier
    {
        return match (CarrierIdentifiers::tryFrom($carrierId)) {
            CarrierIdentifiers::DHL => new DHLCarrier(),
            CarrierIdentifiers::RussianPost => new RussianPostCarrier(),
            default => throw new UnknownCarrierException(),
        };
    }

    public static function makeFromIdentifier(CarrierIdentifiers $carrierId): Carrier
    {
        return static::make($carrierId->value);
    }

    public static function makeFromDelivery(Delivery $delivery): Carrier
    {
        return static::make($delivery->carrier_id);
    }

}
