<?php

namespace App\Models\Carriers;

use App\Contracts\Carrier\CarrierContract;
use App\DTOs\Models\DeliveryDTO;
use App\Enums\CarrierIdentifiers;

abstract class Carrier implements CarrierContract
{
    public const ID = null;

    abstract public function getPriceFor(DeliveryDTO $delivery): int;

    public function getIdentifier(): CarrierIdentifiers
    {
        return static::ID;
    }
}
