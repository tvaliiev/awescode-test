<?php

namespace App\Models\Carriers;

use App\DTOs\Models\DeliveryDTO;
use App\Enums\CarrierIdentifiers;

class DHLCarrier extends Carrier
{
    public const ID = CarrierIdentifiers::DHL;

    public function getPriceFor(DeliveryDTO $delivery): int
    {
        return $delivery->weight * 10;
    }
}
