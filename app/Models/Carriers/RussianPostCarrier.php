<?php

namespace App\Models\Carriers;

use App\DTOs\Models\DeliveryDTO;
use App\Enums\CarrierIdentifiers;

class RussianPostCarrier extends Carrier
{
    public const ID = CarrierIdentifiers::RussianPost;

    public function getPriceFor(DeliveryDTO $delivery): int
    {
        return $delivery->weight <= 10_000 ? 100_00 : 1000_00;
    }
}
