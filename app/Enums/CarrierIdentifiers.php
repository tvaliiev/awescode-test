<?php

namespace App\Enums;

use App\Util\Enum\EnumEnchantments;
use OpenApi\Attributes\Schema;

#[Schema(description: "1 - RussianPost, 2 - DHL", example: 1)]
enum CarrierIdentifiers: int
{
    use EnumEnchantments;

    case RussianPost = 1;
    case DHL = 2;
}
