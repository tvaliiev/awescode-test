<?php

namespace App\Enums;

use App\Util\Enum\EnumEnchantments;
use OpenApi\Attributes\Schema;

#[Schema(default: DeliveryWeightUnitEnum::GRAMS)]
enum DeliveryWeightUnitEnum: string
{
    use EnumEnchantments;

    case KILOGRAMS = 'kg';
    case GRAMS = 'g';
}
