<?php

namespace App\Http\Controllers;

use App\DTOs\Models\CreateDeliveryDTO;
use App\DTOs\Models\UpdateDeliveryDTO;
use App\DTOs\Repositories\DeliveryFilterDTO;
use App\Enums\DeliveryWeightUnitEnum;
use App\Http\Requests\Deliveries\StoreDeliveryRequest;
use App\Http\Requests\Deliveries\UpdateDeliveryRequest;
use App\Http\Resources\DeliveryResource;
use App\Models\Delivery;
use App\Repositories\DeliveryRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use OpenApi\Attributes\Parameter;

class DeliveryController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/deliveries",
     *      operationId="listDeliveries",
     *      tags={"Delivery"},
     *      description="Get list of deliveries",
     *      @OA\Parameter(
     *          name="filter",
     *          in="query",
     *          style="deepObject",
     *          explode=true,
     *          @OA\Schema(ref="#/components/schemas/GetDeliveriesListRequest"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/DeliveryResource")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * )
     * Display a listing of the resource.
     *
     * @throws \Spatie\DataTransferObject\Exceptions\UnknownProperties
     */
    public function index(Request $request, DeliveryRepository $repository): DeliveryResource
    {
        $filter = new DeliveryFilterDTO($request->input('filter'));
        if ($filter->weightUnit === DeliveryWeightUnitEnum::KILOGRAMS && $filter->weight !== null) {
            $filter->weight->convertToGrams();
        }

        $deliveries = $repository->getList($filter);
        return new DeliveryResource($deliveries);
    }

    /**
     * @OA\Post(
     *      path="/api/deliveries",
     *      operationId="createDelivery",
     *      tags={"Delivery"},
     *      summary="",
     *      description="Creates new delivery",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreDeliveryRequest"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/DeliveryResource")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     * )
     *
     * @throws \Spatie\DataTransferObject\Exceptions\UnknownProperties
     */
    public function store(StoreDeliveryRequest $request, DeliveryRepository $deliveryRepository)
    {
        $delivery = $deliveryRepository->store(new CreateDeliveryDTO($request->input('delivery')));
        return (new DeliveryResource($delivery))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Delivery $delivery
     * @return \Illuminate\Http\Response
     */
    public function show(Delivery $delivery)
    {
        //
    }

    /**
     * @OA\Put(
     *     path="/api/deliveries/{id}",
     *     operationId="updateDelivery",
     *     tags={"Delivery"},
     *     summary="",
     *     description="Creates new delivery",
     *     @OA\Parameter(in="path", name="id", description="ID of delivery", required=true, example=12, @OA\Schema(type="integer")),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/StoreDeliveryRequest"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/DeliveryResource")
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Bad Request"
     *      ),
     * )
     * Update the specified resource in storage.
     * @param \App\Http\Requests\Deliveries\UpdateDeliveryRequest $request
     * @throws \Spatie\DataTransferObject\Exceptions\UnknownProperties
     */
    public function update(UpdateDeliveryRequest $request, DeliveryRepository $deliveryRepository, int $delivery)
    {
        $requestData = $request->input('delivery');
        $requestData['id'] = $delivery;

        $delivery = $deliveryRepository->update(new UpdateDeliveryDTO($requestData));
        return new DeliveryResource($delivery);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Delivery $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Delivery $delivery)
    {
        //
    }
}
