<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      x={
 *          "logo": {
 *              "url": "https://via.placeholder.com/190x90.png?text=My Test"
 *          }
 *      },
 *      title="OpenApi",
 *      @OA\Contact(
 *          email="wkill95 at gmail.com"
 *      ),
 *     @OA\License(
 *         name="MIT",
 *     )
 * )
 * @OA\Schema(
 *     schema="DefaultPagination",
 *     @OA\Property(property="current_page"),
 *     @OA\Property(property="total", type="integer"),
 *     @OA\Property(property="per_page", type="integer"),
 *     @OA\Property(property="to", type="integer"),
 *     @OA\Property(property="first_page_url"),
 *     @OA\Property(property="last_page_url"),
 *     @OA\Property(property="prev_page_url"),
 *     @OA\Property(property="next_page_url"),
 *     @OA\Property(property="last_page"),
 *     @OA\Property(property="path"),
 *     @OA\Property(
 *          property="links",
 *          type="array",
 *          @OA\Items(
 *              @OA\Property(property="url", type="string"),
 *              @OA\Property(property="label", type="string"),
 *              @OA\Property(property="active", type="boolean"),
 *          ),
 *     ),
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
