<?php

namespace App\Http\Requests\Deliveries;

use App\Enums\CarrierIdentifiers;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Update delivery",
 *      required={"delivery"},
 *      @OA\Property(
 *          property="delivery",
 *          title="delivery",
 *          type="object",
 *          @OA\Property(
 *              property="weight",
 *              title="weight",
 *              description="Weight in grams",
 *              format="int64",
 *              example="2100",
 *           ),
 *          @OA\Property(
 *              property="carrier_id",
 *              title="carrier_id",
 *              description="Carrier",
 *              ref="#/components/schemas/CarrierIdentifiers"
 *          ),
 *          @OA\Property(
 *              property="description",
 *              title="description",
 *              description="Description of delivery",
 *              example="bla bla",
 *          ),
 *          @OA\Property(
 *              property="delivery_price",
 *              title="delivery_price",
 *              description="shipping cost in kopecks",
 *              type="number",
 *              example="4200",
 *          ),
 *      ),
 * )
 */
class UpdateDeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'delivery.weight' => ['int', 'gt:0'],
            'delivery.description' => [ 'string', 'min:1'],
            'delivery.carrier_id' => ['int', 'in:' . CarrierIdentifiers::valueList()],
            'delivery.delivery_price' => ['int', 'gt:0'],
        ];
    }
}
