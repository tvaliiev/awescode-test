<?php

namespace App\Http\Requests\Deliveries;

use App\Enums\CarrierIdentifiers;
use App\Enums\DeliveryWeightUnitEnum;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Get deliveries request",
 *      @OA\Property(
 *          property="filter",
 *          type="object",
 *              @OA\Property(
 *                  property="weight",
 *                  type="object",
 *                  @OA\Property(property="gt", oneOf={@OA\Schema(type="integer"),@OA\Schema(type="number")}, example="1.3"),
 *                  @OA\Property(property="lt", oneOf={@OA\Schema(type="integer"),@OA\Schema(type="number")}, example="1000"),
 *              ),
 *              @OA\Property(
 *                  property="description",
 *                  type="object",
 *                  @OA\Property(property="like", type="string", example="substring"),
 *              ),
 *              @OA\Property(
 *                  property="carrier_id",
 *                  type="object",
 *                  @OA\Property(property="eq", ref="#/components/schemas/CarrierIdentifiers"),
 *              ),
 *              @OA\Property(
 *                  property="weight_unit",
 *                  ref="#/components/schemas/DeliveryWeightUnitEnum",
 *              ),
 *      )
 * )
 *
 */
class GetDeliveriesListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'filter.carrier_id.eq' => ['in:' . CarrierIdentifiers::valueList()],
            'filter.weight.gt' => ['numeric', 'gt:0'],
            'filter.weight.lt' => ['numeric', 'gt:0'],
            'filter.weight_unit' => ['string', 'in:' . DeliveryWeightUnitEnum::valueList()],
            'filter.delivery_price.gt' => ['int', 'gt:0'],
            'filter.delivery_price.lt' => ['int', 'gt:0'],
            'filter.description.like' => ['string', 'min:1'],
        ];
    }
}
