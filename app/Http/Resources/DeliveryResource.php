<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes\AdditionalProperties;

/**
 * @OA\Schema(
 *     schema="PaginatedDeliveryResouce",
 *     title="PaginatedDeliveryResource",
 *     @OA\Property(
 *         property="data",
 *         title="data",
 *         description="Data wrapper",
 *         type="array",
 *         @OA\Items(ref="#/components/schemas/DeliveryDTO")
 *     ),
 *     @OA\AdditionalProperties(ref="#/components/schemas/DefaultPagination")
 * )
 * @OA\Schema(
 *     title="Single item delivery resouce",
 *     @OA\Property(
 *         property="data",
 *         title="data",
 *         description="Data wrapper",
 *          type="object",
 *          ref="#/components/schemas/DeliveryDTO"
 *     ),
 * )
 */
class DeliveryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
