<?php

namespace App\Console\Commands;

use App\Models\Delivery;
use Illuminate\Console\Command;

class DisplayTestDeliveriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test-deliveries:display';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display batch of deliveries';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $deliveries = Delivery::factory(10)
            ->make()
            ->map(fn(Delivery $delivery) => [__('carriers.' . $delivery->carrier_id), $delivery->weight, $delivery->delivery_price]);
        $this->table(['Carrier', 'Delivery Weight', 'Price'], $deliveries);
        return 0;
    }
}
