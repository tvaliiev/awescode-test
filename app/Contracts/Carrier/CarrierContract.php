<?php

namespace App\Contracts\Carrier;

use App\DTOs\Models\DeliveryDTO;
use App\Enums\CarrierIdentifiers;

interface CarrierContract
{
    public function getIdentifier(): CarrierIdentifiers;

    public function getPriceFor(DeliveryDTO $delivery): int;
}
