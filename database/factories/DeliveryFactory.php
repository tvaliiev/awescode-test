<?php

namespace Database\Factories;

use App\Enums\CarrierIdentifiers;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Delivery>
 */
class DeliveryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'weight' => random_int(500, 5000),
            'carrier_id' => $this->faker->randomElement(CarrierIdentifiers::valueArray()),
            'description' => implode(PHP_EOL, $this->faker->paragraphs),
            'delivery_price' => random_int(500, 10000),
        ];
    }
}
