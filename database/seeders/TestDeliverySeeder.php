<?php

namespace Database\Seeders;

use App\Enums\CarrierIdentifiers;
use App\Models\Delivery;
use Illuminate\Database\Seeder;

class TestDeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templates = [
            [
                'id' => 1,
                'delivery_price' => 100_00,
                'carrier_id' => CarrierIdentifiers::RussianPost->value,
                'weight' => 1000,
            ],
            [
                'id' => 2,
                'delivery_price' => 120_00,
                'carrier_id' => CarrierIdentifiers::RussianPost->value,
                'weight' => 1010,
            ],
            [
                'id' => 3,
                'delivery_price' => 130_00,
                'carrier_id' => CarrierIdentifiers::RussianPost->value,
                'weight' => 1020,
                'description' => 'sldkjf testrp awef',
            ],
            [
                'id' => 4,
                'delivery_price' => 115_00,
                'carrier_id' => CarrierIdentifiers::DHL->value,
                'weight' => 1000,
            ],
            [
                'id' => 5,
                'delivery_price' => 120_00,
                'carrier_id' => CarrierIdentifiers::DHL->value,
                'weight' => 1010,
            ],
            [
                'id' => 6,
                'delivery_price' => 133_00,
                'carrier_id' => CarrierIdentifiers::DHL->value,
                'weight' => 1030,
                'description' => 'sldkjf testdhl awef',
            ],
        ];

        foreach ($templates as $template) {
            Delivery::factory(1, $template)->create();
        }
    }
}
