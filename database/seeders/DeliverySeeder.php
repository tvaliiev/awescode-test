<?php

namespace Database\Seeders;

use App\Models\Delivery;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Delivery::factory(50)
            ->make()
            ->each(function (Delivery $delivery) {
                $delivery->delivery_price = $delivery->calculateDeliveryPrice();
                $delivery->save();
            });
    }
}
