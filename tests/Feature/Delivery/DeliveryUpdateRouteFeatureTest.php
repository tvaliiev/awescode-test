<?php

namespace Tests\Feature\Delivery;

use App\Enums\CarrierIdentifiers;
use App\Models\Delivery;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeliveryUpdateRouteFeatureTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdateExistingDelivery()
    {
        /** @var Delivery $delivery */
        $delivery = Delivery::factory([
            'carrier_id' => CarrierIdentifiers::DHL->value,
        ])->create();

        $deliveryNewData = [
            'weight' => 400,
            'delivery_price' => 100,
            'description' => 'bla bla',
            'carrier_id' => CarrierIdentifiers::RussianPost->value,
        ];
        $this->put('/api/deliveries/' . $delivery->getKey(), [
            'delivery' => $deliveryNewData,
        ])
            ->assertStatus(200)
            ->assertJson(
                fn(AssertableJson $json) => $json
                    ->has(
                        'data',
                        fn(AssertableJson $json) => $json
                            ->whereAll($deliveryNewData)
                            ->where('id', $delivery->getKey())
                    )
            );
    }
}
