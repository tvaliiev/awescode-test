<?php

namespace Tests\Feature\Delivery;

use App\Enums\CarrierIdentifiers;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeliveryCreateRouteFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateDelivery()
    {
        $this->postJson('/api/deliveries', [
            'delivery' => [
                'weight' => 1000,
                'description' => 'bla bla',
                'carrier_id' => CarrierIdentifiers::RussianPost->value,
            ],
        ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJson(
                fn(AssertableJson $json) => $json
                    ->has(
                        'data',
                        fn(AssertableJson $json) => $json
                            ->whereAll([
                                'id' => 1,
                                'weight' => 1000,
                                'description' => 'bla bla',
                                'delivery_price' => 100_00,
                                'carrier_id' => CarrierIdentifiers::RussianPost->value,
                            ])
                    )
            );
    }

    public function testCreateInvalidDelivery()
    {
        $this->postJson('/api/deliveries', [
            'delivery' => [
                'weight' => 0,
                'description' => '',
                'carrier_id' => 0,
            ],
        ])->assertInvalid([
            'delivery.weight' => 'delivery.weight must be greater than 0',
            'delivery.description' => 'required',
            'delivery.carrier_id' => 'invalid',
        ]);
    }
}
