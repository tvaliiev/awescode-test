<?php

namespace Tests\Feature\Delivery;

use App\Enums\CarrierIdentifiers;
use App\Models\Delivery;
use Database\Seeders\TestDeliverySeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeliveryIndexRouteFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function testHasBaseStructure()
    {
        Delivery::factory(5)->create();
        $this->get('/api/deliveries')
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(
                fn(AssertableJson $json) => $json
                    ->whereAll([
                        'current_page' => 1,
                        'total' => 5,
                    ])
                    ->hasAll([
                        'first_page_url',
                        'from',
                        'last_page',
                        'last_page_url',
                        'links',
                        'next_page_url',
                        'path',
                        'per_page',
                        'prev_page_url',
                        'to',
                    ])
                    ->whereType('data', 'array')
                    ->has(
                        'data.0',
                        fn(AssertableJson $json) => $json
                            ->hasAll([
                                'id',
                                'carrier_id',
                                'weight',
                                'description',
                                'delivery_price',
                            ]))
                    ->count('data', 5)

            );
    }

    /**
     * @dataProvider filterByEmptyFilter
     * @dataProvider filterByWeight
     * @dataProvider filterByCarrier
     * @dataProvider filterByDescription
     * @dataProvider filterByDeliveryPrice
     * @dataProvider filterByCombined
     *
     * @param array $parameters
     * @param array $expectedIds
     * @return void
     */
    public function testIndexFilterForDeliveries(array $parameters, array $expectedIds)
    {
        $this->seed(TestDeliverySeeder::class);

        $this->call('GET', '/api/deliveries', $parameters)
            ->assertJson(
                fn(AssertableJson $json) => $json
                    ->count('data', count($expectedIds))
                    ->whereAll(
                        collect($expectedIds)->mapWithKeys(fn(int $value, int $key) => ["data.$key.id" => $value])->toArray()
                    )
                    ->etc()
            );
    }

    public function filterByEmptyFilter(): array
    {
        return [
            [
                [
                ],
                [1, 2, 3, 4, 5, 6],
            ],
        ];
    }

    public function filterByWeight(): array
    {
        return [
            [
                [
                    'filter' => [
                        'weight' => [
                            'gt' => 1,
                            'lt' => 1.03,
                        ],
                        'weightUnit' => 'kg',
                    ],
                ],
                [2, 3, 5],
            ],
            [
                [
                    'filter' => [
                        'weight' => [
                            'gt' => 1000,
                            'lt' => 1030,
                        ],
                    ],
                ],
                [2, 3, 5],
            ],
            [
                [
                    'filter' => [
                        'weight' => [
                            'eq' => 1000,
                        ],
                    ],
                ],
                [1, 4],
            ],
            [
                [
                    'filter' => [
                        'weight' => [
                            'eq' => 1000,
                        ],
                        'weightUnit' => 'g',
                    ],
                ],
                [1, 4],
            ],
        ];
    }

    public function filterByCarrier(): array
    {
        return [
            [
                [
                    'filter' => [
                        'carrier_id' => ['eq' => CarrierIdentifiers::DHL->value],
                    ],
                ],
                [4, 5, 6],
            ],
            [
                [
                    'filter' => [
                        'carrier_id' => ['eq' => CarrierIdentifiers::RussianPost->value],
                    ],
                ],
                [1, 2, 3],
            ],
        ];
    }

    public function filterByDescription(): array
    {
        return [
            [
                [
                    'filter' => [
                        'description' => ['like' => 'test'],
                    ],
                ],
                [3, 6],
            ],
            [
                [
                    'filter' => [
                        'description' => ['like' => 'testdhl'],
                    ],
                ],
                [6],
            ],
            [
                [
                    'filter' => [
                        'description' => [
                            'like' => 'awef',
                        ],
                    ],
                ],
                [3, 6],
            ],
        ];
    }

    public function filterByDeliveryPrice(): array
    {
        return [
            [
                [
                    'filter' => [
                        'delivery_price' => ['lt' => 130_00, 'gt' => 100_00],
                    ],
                ],
                [2, 4, 5],
            ],
        ];
    }

    public function filterByCombined(): array
    {
        return [
            [
                [
                    'filter' => [
                        'description' => [
                            'like' => 'test',
                        ],
                        'delivery_price' => [
                            'lt' => 132_00,
                        ],
                    ],
                ],
                [3],
            ],
            [
                [
                    'filter' => [
                        'weight' => [
                            'gt' => 1,
                            'lt' => 1.03,
                        ],
                        'weightUnit' => 'kg',
                        'delivery_price' => [
                            'lt' => 125_00,
                        ]
                    ],
                ],
                [2, 5],
            ],
        ];
    }
}
