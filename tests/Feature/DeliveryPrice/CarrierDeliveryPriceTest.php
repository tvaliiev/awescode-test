<?php

namespace Tests\Feature\DeliveryPrice;

use App\Enums\CarrierIdentifiers;
use App\Models\Delivery;
use Tests\TestCase;

class CarrierDeliveryPriceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @dataProvider deliveriesWithDHLCarrier
     * @dataProvider deliveriesWithRussianPostCarrier
     * @return void
     */
    public function test_if_delivery_price_match(array $delivery, float $expectedPrice)
    {
        $delivery = Delivery::factory($delivery)->make();

        $this->assertEquals(
            $expectedPrice,
            $delivery->calculateDeliveryPrice(),
            "Price for carrier {$delivery['carrier_id']->value} does not match expected value",
        );
    }

    public function deliveriesWithRussianPostCarrier()
    {
        return [
            [['weight' => 5_000, 'carrier_id' => CarrierIdentifiers::RussianPost->value], 100_00],
            [['weight' => 1_0000, 'carrier_id' => CarrierIdentifiers::RussianPost->value], 100_00],
            [['weight' => 50_000, 'carrier_id' => CarrierIdentifiers::RussianPost->value], 1000_00],
        ];
    }

    public function deliveriesWithDHLCarrier()
    {
        return [
            [['weight' => 1_000, 'carrier_id' => CarrierIdentifiers::DHL->value], 100_00],
            [['weight' => 12_000, 'carrier_id' => CarrierIdentifiers::DHL->value], 1200_00],
        ];
    }
}
