<?php

use App\Enums\CarrierIdentifiers;

return [
    CarrierIdentifiers::RussianPost->value => 'Russian Post',
    CarrierIdentifiers::DHL->value => 'DHL',
];
